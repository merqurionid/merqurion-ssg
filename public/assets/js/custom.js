var vis = (function(){
    var stateKey, eventKey, keys = {
        hidden: "visibilitychange",
        webkitHidden: "webkitvisibilitychange",
        mozHidden: "mozvisibilitychange",
        msHidden: "msvisibilitychange"
    };
    for (stateKey in keys) {
        if (stateKey in document) {
            eventKey = keys[stateKey];
            break;
        }
    }
    return function(c) {
        if (c) document.addEventListener(eventKey, c);
        return !document[stateKey];
    }
})();

var visible = vis(); 

$(function() {
    const MENU_HOME = 'home';
    const MENU_SERVICE = 'service';
    const MENU_AGILE = 'agile';
    const MENU_CONTACT = 'contact';
    
    var active_menu = MENU_HOME;

    var text_primary_color = 'rgb(112, 112, 112)';
    var text_secondary_color = 'rgb(148, 167, 187)';

    var topOffset;
    var sd_Offset;
    var si_Offset;
    var im_Offset;
    var ta_Offset;

    var ServiceOffset;
    var AgileOffset;

    var screenHeight;

    var bottomOffset;

    resetOffsets();

    changeActiveMenu(topOffset);

    setNavbarStyle(topOffset);
    setServiceStyle(bottomOffset);

    function resetOffsets() {
        topOffset = $(window).scrollTop();

        sd_Offset = $('#software-development').offset().top;
        si_Offset = $('#system-integration').offset().top;
        im_Offset = $('#information-management').offset().top;
        ta_Offset = $('#technology-advisory').offset().top;

        ServiceOffset = $('#services').offset().top;
        AgileOffset = $('#agile').offset().top;

        screenHeight = $(window).height();

        bottomOffset = topOffset + screenHeight;
    }

    function resetStyles() {
        resetOffsets();

        changeActiveMenu(topOffset);
        setNavbarStyle(topOffset);
        setServiceStyle(bottomOffset);
    }

    $(window).resize(function() {
        resetStyles();
    });

    $(window).scroll(function() {
        resetStyles();
	});

    function changeActiveMenu(topOffset) {
        if(topOffset >= ServiceOffset - 200 && topOffset < AgileOffset - 400) {
            active_menu = MENU_SERVICE;
            setActiveMenu();
        } 
        if(topOffset >= AgileOffset - 400) {
            active_menu = MENU_AGILE;
            setActiveMenu();
        }
        if(topOffset + $(window).height() >= $(document).height() - 5) {
            active_menu = MENU_CONTACT;
            setActiveMenu();
        }  
    }

    function setServiceStyle(bottomOffset) {
        if(bottomOffset >= sd_Offset + 150) {
            $('#software-development img').animate({
                'margin-left': '0'
            }, 600);
            $('#software-development div').animate({
                'margin-right': '0'
            }, 600);
        }
        if(bottomOffset >= si_Offset + 150) {
            $('#system-integration img').animate({
                'margin-right': '0'
            }, 600);
            $('#system-integration div').animate({
                'margin-left': '0'
            }, 600);
        }
        if(bottomOffset >= im_Offset + 150) {
            $('#information-management img').animate({
                'margin-left': '0'
            }, 600);
            $('#information-management div').animate({
                'margin-right': '0'
            }, 600);
        }
        if(bottomOffset >= ta_Offset + 150) {
            $('#technology-advisory img').animate({
                'margin-right': '0'
            }, 600);
            $('#technology-advisory div').animate({
                'margin-left': '0'
            }, 600);
        }
    }

    function setNavbarStyle(topOffset) {
        if(topOffset > 0) {
            $('#navbar').css({'background-color': 'rgba(255,255,255,1)'})
            $('#navbar div #logo img').css({'transform': 'scale(1, 1)'});
            $('#navbar div #logo div').css({'transform': 'translateX(0)'});
            $('#navbar div #logo div h1').css({'color': text_primary_color});
            $('#navbar div #logo div h2').css({'color': text_secondary_color});
            $('#menus ul li a').css({'color': text_primary_color});
            $('#menus ul li a .menu-border').css({'border-bottom': `2px solid ${text_primary_color}`});
            $('#menus-mobile-button-show').css({'color': text_primary_color});
            $('#menus-mobile-button-hide').css({'color': text_primary_color});
        } else {
            active_menu = MENU_HOME;
            setActiveMenu();

            $('#navbar').css({'background-color': 'rgba(255,255,255,0)'})
            $('#navbar div #logo img').css({'transform': 'scale(0, 0)'});
            $('#navbar div #logo div').css({'transform': 'translateX(-40%)'});
            $('#navbar div #logo div h1').css({'color': 'white'});
            $('#navbar div #logo div h2').css({'color': 'white'});
            $('#menus ul li a').css({'color': 'white'});
            $('#menus ul li a .menu-border').css({'border-bottom': `2px solid white`});
            $('#menus-mobile-button-show').css({'color': 'white'});
            $('#menus-mobile-button-hide').css({'color': 'white'});
        }
    }

    //---------------------------------------------------------------------------------navbar

    $('#menus ul li').each(function () {
        var $this = $(this);
        $this.on("click", function (e) {
            scrollToActivePage(e, $this);
        });
    });

    $('#menus-mobile ul li').each(function () {
        var $this = $(this);
        $this.on("click", function (e) {
            scrollToActivePage(e, $this);
        });
    });

    function scrollToActivePage(event, elemement) {
        event.preventDefault();
        active_menu = elemement.attr('data-menu');
        setActiveMenu();

        moveScroll();
    }

    function moveScroll() {
        if(active_menu === MENU_HOME) {
            $('body, html').animate({
                scrollTop: 0
            }, 500)
        }
        if(active_menu === MENU_SERVICE) {
            $('body, html').animate({
                scrollTop: ServiceOffset - 200
            }, 500)
        }
        if(active_menu === MENU_AGILE) {
            $('body, html').animate({
                scrollTop: AgileOffset - 150
            }, 500)
        }
        if(active_menu === MENU_CONTACT) {
            $('body, html').animate({
                scrollTop: AgileOffset + 1000
            }, 500)
        }
    }

    $('#menus-mobile-button-show').on("click", function () {
        showMenuMobile();
    });

    $('#menus-mobile-button-hide').on("click", function () {
        hideMenuMobile();
    });

    function showMenuMobile() {
        $('#menus-mobile-button-show').css({'display': 'none'});
        $('#menus-mobile-button-hide').css({'display': 'block'});
        $('#menus-mobile').css({'transform': 'translateX(0)'});
        
        setActiveMenu();
    }
    
    function hideMenuMobile() {
        $('#menus-mobile-button-show').css({'display': 'block'});
        $('#menus-mobile-button-hide').css({'display': 'none'});
        $('#menus-mobile').css({'transform': 'translateX(150%)'});
    }

    function setActiveMenu() {
        var activated = $('.active-menu');
        var activated_mobile = $('.active-menu-mobile');
      
        if(activated.length>0) 
          activated.removeClass();
        
        if(activated_mobile.length>0) 
          activated_mobile.removeClass();
        
      
        $('#menus ul').find(`[data-menu='${active_menu}']`).addClass('active-menu');
        $('#menus-mobile ul').find(`[data-menu='${active_menu}']`).addClass('active-menu-mobile');
    }

    //-----------------------------------------------------------------------------banner

    var bannerTextAnim;

    runBannerTextAnim()

    vis(function(){
        if(!vis())
            clearInterval(bannerTextAnim) 
        else
            runBannerTextAnim()
    });

    index = 0;
    $('#banner-text div span').css({'opacity':'0', 'margin-left': '10%'});
    $('#banner-text div span:eq(' + index + ')').css({'opacity':'1', 'margin-left': ''});

    function runBannerTextAnim() {
        bannerTextAnim = setInterval(function() {
            index += 1;
            if(index > 2) {
                index = 0;
            };
    
            $('#banner-text div span').animate({
                'opacity':'0', 
                'margin-left': '10%'
            }, 200);
    
            $('#banner-text div span:eq(' + index + ')').animate({
                'opacity':'1', 
                'margin-left': ''
            }, 200);
        }, 3000);
    }

    //---------------------------------------------------------------------------------agile

    initialCarousel();

    $(window).resize(function() {
        $("#agile #agile-list #items").slick('unslick')
        initialCarousel();
    });

    function initSlickPlugin(slidesCount) {
        $("#agile #agile-list #items").slick({
            dots: true,
            arrows: false,
            infinite: true,
            slidesToShow: slidesCount,
            slidesToScroll: slidesCount
        });
    }
        
    function initialCarousel() {
        var screenWidth = $( window ).width();

        if(screenWidth > 800) { 
            initSlickPlugin(3);
        } else if(screenWidth <= 800 && screenWidth >= 450) {
            initSlickPlugin(2);
        } else if(screenWidth < 450) {
            initSlickPlugin(1);
        }
    } 
})