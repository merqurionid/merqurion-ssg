import {Navbar, Banner, Service, Agile, Contact} from '../components/layout'

export default function Home() {
  return (
    <>
      <Navbar/>
      <Banner/>
      <Service/>
      <Agile/>
      <Contact/>
    </>
  )
}
