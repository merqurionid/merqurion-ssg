import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
    render() {
        return (
            <html lang='en'>
            <Head>
                <title>Merqurion</title>
                <meta name="description" content="Merqurion Solutions" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
                <link rel="icon" href="/favicon.ico" />
                <link rel="stylesheet" type="text/css" href="assets/plugin/slick/slick.css"/>
                <link rel="stylesheet" type="text/css" href="assets/plugin/slick/slick-theme.css"/>
                <script src='assets/js/jquery.js'></script>
            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
            <script src='assets/js/custom.js'></script>
            <script src="assets/plugin/slick/slick.js" type="text/javascript"></script>
            </html>
        )
    }
}
