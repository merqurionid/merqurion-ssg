export default function Agile() {
  return <div id='agile'>
    <h1>Agile Framework</h1>
    <h3>Flexible iterative method in the execution of work, by providing joint control and supervision between owner and executor, enabling product and service to be delivered with expected quality and time.</h3>
    <div id='agile-list'>
      <div id="items">
        <div>
          <div className='agile-content'>
            {/* <img src="assets/logo.png" alt=""/> */}
            <h4>Planning</h4>
            <p>Formulate agile team, plan the project roadmap, manage value and priority on an ongoing basis, plan the sprint agenda.</p>
          </div>
        </div>
        <div>  
          <div className='agile-content'>
            {/* <img src="assets/logo.png" alt=""/> */}
            <h4>Execution</h4>
            <p>Execute task in the sprint, manage assignment and progress, carry out daily coordination, conduct internal quality assurance.</p>
          </div>
        </div>
        <div>  
          <div className='agile-content'>
            {/* <img src="assets/logo.png" alt=""/> */}
            <h4>Evaluation</h4>
            <p>Deliver periodically based on the sprint schedule, evaluate sprint result, receive constructive feedback, respond to change.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
}