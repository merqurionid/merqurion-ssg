import Navbar from './navbar'
import Banner from './banner'
import Service from './service'
import Agile from './agile'
import Contact from './contact'

export {Navbar, Banner, Service, Agile, Contact}