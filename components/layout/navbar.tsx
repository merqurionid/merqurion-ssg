import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons"; // import the icons you need

export default function Navbar() {
  return (
    <div id="navbar">
      <div>
        <a id='logo' href=''>
          <img src='assets/logo.png'/>
          <div>
            <h1>merqurion</h1>
            <h2>solutions</h2>
          </div>
        </a>
        <div id='menus'>
          <ul>
            <li data-menu='home' className='active-menu'><a href=''>Home<span className='menu-border'></span></a></li>
            <li data-menu='service'><a href=''>Services<span className='menu-border'></span></a></li>
            <li data-menu='agile'><a href=''>Agile<span className='menu-border'></span></a></li>
            <li data-menu='contact'><a href=''>Contact<span className='menu-border'></span></a></li>
          </ul>
        </div>
        <div id='menus-mobile'>
          <ul>
            <li data-menu='home' className='active-menu-mobile'><a href=''>Home</a></li>
            <li data-menu='service'><a href=''>Services</a></li>
            <li data-menu='agile'><a href=''>Agile</a></li>
            <li data-menu='contact'><a href=''>Contact</a></li>
          </ul>
        </div>
        <span id='menus-mobile-button-show'>
          <FontAwesomeIcon icon={faBars}></FontAwesomeIcon>
        </span>
        <span id='menus-mobile-button-hide'>
          <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
        </span>
      </div>
    </div>
  )
}