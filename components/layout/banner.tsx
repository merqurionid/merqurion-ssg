export default function Banner() {
  return (
    <div id='banner'>
      <img id='banner-bg' src="assets/banner.png" alt=""/>
      <div id='banner-text'>
        We Provide
        <div>
          Most&nbsp;
          <span> <b>Creative</b> </span>
          <span> <b>Qualified</b> </span>
          <span> <b>Valuable</b> </span>
        </div>
        Solutions
        <div id='banner-btn'><a href="">Know More</a></div>
      </div>
    </div>
  )
}