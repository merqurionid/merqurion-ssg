export default function Service() {
  return <div>
    <div id='services'>
      <div id='service-section'>
        <div id="service-statement">
          <p> <b>Merqurion is a limited liability digital company that provides consulting and development in information technology.</b> </p>
          <br/>
          <p> <b>Merqurion focuses on providing dedicated service for any kind of industries to improve the quality of the business processes.</b> </p>
        </div>
        <section id='software-development' className='orientation-left'>
          <img src="assets/software_development.png" alt=""/>
          <div>
            <h2>Software Development</h2>
            <br/><br/>
            <p>Merqurion has partner with the best consultants and engineers, also experienced with wide range of technology and platform.</p>
            <p>Merqurion adopts the agile principles in the software development process to provide the highest delivery value for client.</p>
          </div>
        </section>
        <section id='system-integration' className='orientation-right'>
          <div>
            <h2>System Integration</h2>
            <br/><br/>
            <p>Merqurion provides customized products and services for business processes integration in order to improve the management efficiency.</p>
            <p>Merqurion has hands on experiences in design and implementation of service oriented architecture and integrated microservice orchestration.</p>
          </div>
          <img src="assets/system_integration.png" alt=""/>
        </section>
        <section id='information-management' className='orientation-left'>
          <img src="assets/information_management.png" alt=""/>
          <div>
            <h2>Information Management</h2>
            <br/><br/>
            <p>Merqurion collaborates with expert data scientist to acquire information and knowledge tha increase the company competitiveness.</p>
            <p>Merqurion has expertise in selecting and configuring the right technology stack that maches the specific criteria of each company.</p>
          </div>
        </section>
        <section id='technology-advisory' className='orientation-right'>
          <div>
            <h2>Technology Advisory</h2>
            <br/><br/>
            <p>Merqurion offers advisory services in information technology analysis, wheter at the investment planning phase or at the revolution stage.</p>
            <p>Merqurion cooperates with certified consultants to develop efficient information technology fovernance that supports the company core businesses.</p>
          </div>
          <img src="assets/technology_advisory.png" alt=""/>
        </section>
      </div>
    </div>
    <div id='service-model'>
      <h1>Service Model</h1>
      <h2>Lorem Ipsum Terbaik The Best</h2>
    </div>
  </div>
}
  
