export default function Contact() {
  return <div id="contact">
    <div id='company-name'>
      <div>
        <h1><b>merqurion</b></h1>
        <h2>solutions</h2>
      </div>
      <h4>PT. Merkurius Solusi Terpadu</h4>
    </div>
    <div  id='company-address'>
      <h4>Jl. Saturnus Selatan 8, No. 6 <br/> Bandung, Indonesia, 40286</h4>
      <br/>
      <h5>Email: adm@merqurion.com</h5>
      <h5>Website: www.merqurion.com</h5>
    </div>
  </div>
}